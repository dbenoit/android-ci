FROM ubuntu:yakkety
MAINTAINER David BENOIT <david@sogilis.com>

ENV ANDROID_TARGET_SDK="24" \
    ANDROID_BUILD_TOOLS="24.0.0" \
    ANDROID_SDK_TOOLS="24.4.1"
 
ENV DEBIAN_FRONTEND noninteractive
	
RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes openjdk-8-jdk

RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1

#KVM installation
RUN apt-get --quiet install --yes qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils

#SDK download
RUN wget --quiet --output-document=android-sdk.tgz https://dl.google.com/android/android-sdk_r${ANDROID_SDK_TOOLS}-linux.tgz && \
    tar --extract --gzip --file=android-sdk.tgz

#SDK update
RUN echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter android-${ANDROID_TARGET_SDK} && \
    echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter platform-tools && \
    echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter build-tools-${ANDROID_BUILD_TOOLS}
RUN echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-android-m2repository && \
    echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-google-google_play_services && \
    echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-google-m2repository

#License management
RUN mkdir "android-sdk-linux/licenses"
RUN echo -e "\n8933bad161af4178b1185d1a37fbf41ea5269c55" > "android-sdk-linux/licenses/android-sdk-license"
RUN echo -e "\n504667f4c0de7af1a06de9f4b1727b84351f2910" > "android-sdk-linux/licenses/android-sdk-preview-license"

#Android Virtual Device creation
RUN echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter sys-img-x86-google_apis-${ANDROID_TARGET_SDK}
RUN echo n | android-sdk-linux/tools/android create avd -n test -t 1 --abi google_apis/x86

#Setting environment variables
ENV ANDROID_HOME=$PWD/android-sdk-linux
ENV ANDROID_AVD_HOME=/root/.android/avd
ENV SHELL=/bin/bash
