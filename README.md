# Instrumented Android tests in Docker container
It all started with this article: http://www.greysonparrelli.com/post/setting-up-android-builds-in-gitlab-ci-using-shared-runners/.
## Docker image creation
We can't use a private project.

**Source:** https://gitlab.com/gitlab-org/gitlab-ce/issues/19275
### KVM installation
> KVM (Kernel Virtual Machine) is a Linux kernel module that allows a user space program to utilize the hardware virtualization features of various processors.

**Source:** http://wiki.qemu.org/KVM
> * libvirt-bin provides libvirtd which you need to administer qemu and kvm instances using libvirt
> * qemu-kvm (kvm in Karmic and earlier) is the backend
> * ubuntu-vm-builder powerful command line tool for building virtual machines
> * bridge-utils provides a bridge from your network to the virtual machine

**Source:** https://stackoverflow.com/questions/67699/how-to-clone-all-remote-branches-in-git

### License management
For apparently legal reasons we can't automatically accept the licenses so I'd to copy hashes (SHA-1) located at `Android SDK home path/licenses` on my machine. Let's hope for a more stable solution.

**Source:** https://stackoverflow.com/questions/38096225/automatically-accept-all-sdk-licences
### SDK update
I download the latest tools and the platforms I need.
### Creation of the emulator
For testing purposes I've cloned the following project: https://github.com/googlesamples/android-testing. I've used the basic sample to simply run well-written instrumented tests inside a Docker container.  The only system image I download is  `sys-img-x86-google_apis-${ANDROID_TARGET_SDK}`. 

The target is “Google APIs” because of the basic sample I use. I faced an error with the message `Waited for the root of the view hierarchy to have window focus and not be requesting layout for over 10 seconds.` and the non-use of this target seems to be one of the causes.

**Source:** https://riggaroo.co.za/setting-circle-ci-test-deploy-build-android-app/
## Image use
You just have to add a file named `.gitlab-ci.yml` with the following content at the root of your project. The emulator will be launched in “No window” mode (**source:** https://paulemtz.blogspot.fr/2013/05/android-testing-in-headless-emulator.html). You will have to wait for it to be fully booted (**source:** https://spin.atomicobject.com/2016/03/10/android-test-script/).
```
image: registry.gitlab.com/dbenoit/android-ci:latest
before_script:
  - chmod +x ./gradlew
  
build:
  script:
    - $ANDROID_HOME/tools/emulator -avd test -noaudio -no-window -gpu off -qemu -m 1024 -enable-kvm &
    - WAIT_CMD="$ANDROID_HOME/platform-tools/adb wait-for-device shell getprop init.svc.bootanim"
    - until $WAIT_CMD | grep -m 1 stopped; do echo "Waiting..."; sleep 1; done
    - $ANDROID_HOME/platform-tools/adb logcat -c
    - ./gradlew --full-stacktrace --debug cAT > log 2>&1
  artifacts:
    when: always
    paths:
    - log
```
**Source:** https://docs.gitlab.com/ce/ci/yaml/README.html
## Supplementary sources
* https://circleci.com/docs/android/